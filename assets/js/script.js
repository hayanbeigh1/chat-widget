class Chat {
  messages = [];

  constructor(chats_container_id) {
    this.chat_container = document.getElementById(chats_container_id);
  }

  render_messages() {
    this.chat_container.innerHTML = "";

    this.messages.forEach((cur) => {
      console.log(cur);
      let html;

      if (cur.type === "inc") {
        html = `<div class="incoming-chat__container">
            <div class="incoming-chat__avatar-container">&nbsp;</div>

            <div class="incoming-chat__chat-container">
              <p class="incoming-chat__text">
                ${cur.message}
              </p>
            </div>
          </div>`;
      } else if (cur.type === "otg") {
        html = `<div class="outgoing-chat__container">
        <div class="outgoing-chat__text-container">
          <p class="outgoing-chat__text">
            ${cur.message}
          </p>
        </div>
      </div>`;
      }

      this.chat_container.insertAdjacentHTML("beforeend", html);
    });

    this.chat_container.scrollTop = this.chat_container.scrollHeight;
  }

  receive_message(message) {
    const message_obj = {
      type: "inc",
      message: message,
    };

    this.messages.push(message_obj);
    this.render_messages();
  }

  send_message(message) {
    const message_obj = {
      type: "otg",
      message: message,
    };

    this.messages.push(message_obj);
    this.render_messages();
  }
}

(function () {
  const msg_ins = new Chat("chats-container");

  msg_ins.send_message("Hey there");
  msg_ins.send_message("how are you?");
  msg_ins.receive_message("I am fine...");

  const input_el = document.getElementById("input-message");
  const send_button_el = document.getElementById("message-send-button");

  function send_message() {
    const message = input_el.value;

    if (message.length > 0) {
      msg_ins.send_message(message);

      input_el.value = "";
    }
  }

  if (input_el && send_button_el) {
    input_el.addEventListener("keypress", (e) => {
      console.log(e);
      if (e.code === "Enter") {
        send_message();
      }
    });

    send_button_el.addEventListener("click", (e) => {
      e.preventDefault();

      send_message();
    });
  }
})();
